const slider1 = document.getElementById("input-1");
const slider2 = document.getElementById("input-2");
const slider3 = document.getElementById("input-3");
const slider4 = document.getElementById("input-4");
const slider5 = document.getElementById("input-5");
const slider6 = document.getElementById("input-6");
const slider7 = document.getElementById("input-7");
const slider8 = document.getElementById("input-8");
const slider9 = document.getElementById("input-9");

//slider function
function onChange(e, inputName) {
  const x = e.target.value;

  if (x == 0) {
    inputName.className = "emoji_0";
  } else if (x == 1) {
    inputName.className = "";
  } else if (x == 2) {
    inputName.className = "emoji_2";
  } else if (x == 3) {
    inputName.className = "emoji_3";
  } else if (x == 4) {
    inputName.className = "emoji_4";
  }
}

slider1.addEventListener("input", function (e) {
  onChange(e, slider1);
});
slider2.addEventListener("input", function (e) {
  onChange(e, slider2);
});
slider3.addEventListener("input", function (e) {
  onChange(e, slider3);
});
slider4.addEventListener("input", function (e) {
  onChange(e, slider4);
});
slider5.addEventListener("input", function (e) {
  onChange(e, slider5);
});
slider6.addEventListener("input", function (e) {
  onChange(e, slider6);
});
slider7.addEventListener("input", function (e) {
  onChange(e, slider7);
});
slider8.addEventListener("input", function (e) {
  onChange(e, slider8);
});
slider9.addEventListener("input", function (e) {
  onChange(e, slider9);
});

// input-1
/*
const range1 = document.querySelector('#input-1');
const div1 = document.querySelector('#moji-1');
const mojis = ['☹️','😑','😐','🙂','😄'];

range1.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div1.textContent = mojis[rangeValue];
});

// input-2

const range2 = document.querySelector('#input-2');
const div2 = document.querySelector('#moji-2');

range2.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div2.textContent = mojis[rangeValue];
});

// input-3

const range3 = document.querySelector('#input-3');
const div3 = document.querySelector('#moji-3');

range3.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div3.textContent = mojis[rangeValue];
});

// input-4

const range4 = document.querySelector('#input-4');
const div4 = document.querySelector('#moji-4');

range4.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div4.textContent = mojis[rangeValue];
});

// input-5

const range5 = document.querySelector('#input-5');
const div5 = document.querySelector('#moji-5');

range5.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div5.textContent = mojis[rangeValue];
});

// input-6

const range6 = document.querySelector('#input-6');
const div6 = document.querySelector('#moji-6');

range6.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div6.textContent = mojis[rangeValue];
});

// input-7

const range7 = document.querySelector('#input-7');
const div7 = document.querySelector('#moji-7');

range7.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div7.textContent = mojis[rangeValue];
});

// input-8

const range8 = document.querySelector('#input-8');
const div8 = document.querySelector('#moji-8');

range8.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div8.textContent = mojis[rangeValue];
});

// input-9

const range9 = document.querySelector('#input-9');
const div9 = document.querySelector('#moji-9');

range9.addEventListener('input', (e) => {
  let rangeValue = e.target.value;
  div9.textContent = mojis[rangeValue];
});

*/
