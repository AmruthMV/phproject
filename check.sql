CREATE TABLE `contact` (
  `Sno` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `type` varchar(11) NOT NULL,
  `passed_year` varchar(5) NOT NULL,
  `subject` varchar(215) NOT NULL,
  `Referred_by` varchar(12) NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `contact` (`Sno`, `name`, `email`, `phone`, `type`, `passed_year`, `subject`, `Referred_by`, `message`) VALUES
(1, 's', 's@gmail.com', '+91956841287', 'Fresher', '2022', 'w', 'School', '2wwww');
